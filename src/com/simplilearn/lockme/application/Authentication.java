package com.simplilearn.lockme.application;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import com.simplilearn.lockme.model.UserSiteCredentials;
import com.simplilearn.lockme.model.Users;

public class Authentication {
	private static final String LoggedInUser = null;
	//input data
	private static Scanner keyboard;
	private static Scanner input;
	private static Scanner lockerinput;
	//output data
	private static PrintWriter output;
	private static PrintWriter lockerOutput;
	//model to store data
	private static Users users;
	private static UserSiteCredentials userCredentials;
	

	public static void main(String[] args) {
		initApp();
		welcomeScreen();
		signInOptions();
		
		
	}
	
	public static void signInOptions() {
		System.out.println();
		System.out.println("Hellp, please choose what operations you want to perform from below options.");
		System.out.println("1. Registration ");
		System.out.println("2. Login ");
		String option=keyboard.next();
		switch(option) {
		case "1":
		case "Registration" :
			registerUser();
			break;
		case "2":
		case "Login":
			loginUser();
			break;
		default:
			System.out.println("Please select from either option 1 or 2");
			break;
		
		}
		
		keyboard.close();
		input.close();
		
	}
	
	//public static void validateExistingUser(String LoggedInUser) {
		//if() {
			//System.out.println(" User already exists. Please login with your existing user name");
	//}
	//}
	
	public static void lockerOptions(String inpUsername) {
		System.out.println("1. FETCH ALL STORED CREDENTIALS ");
		System.out.println("2. STORED CREDENTIALS ");
		String option=keyboard.next();
		switch(option) {
		case "1":
			fetchCredentials(inpUsername);
			
			break;
		case "2":
		
			storeCredentials(inpUsername);
			break;
		default:
			System.out.println("Please select either 1 or 2");
			break;
		
		}
		
		keyboard.close();
		lockerinput.close();
		
	}
	
	public static void loginUser() {
		System.out.println("====================================");
		System.out.println("====================================");
		System.out.println("*                                   *");
		System.out.println("*     WELCOME TO LOGIN PAGE   :     *");
		System.out.println("*                                   *");
		System.out.println("====================================");
		System.out.println("====================================");
		
		System.out.println(" Enter Username : ");
		String inpUsername=keyboard.next();
		boolean found=false;
		while(input.hasNext() && !found) {
			if(input.next().equals(inpUsername)) {
				System.out.println(" Enter Password :");
				
				String inpPassword=keyboard.next();
				if(input.next().equals(inpPassword)) {
				System.out.println("Login successful ! 200 OK");
				found=true;
				lockerOptions(inpUsername);
				break;
				}
			}
		}
		
		if(!found) {
			System.out.println("User Not Found : Login Failure : 404 ");
		}
		
		
		
		
	}

	public static void registerUser() {
		System.out.println("====================================");
		System.out.println("====================================");
		System.out.println("*                                   *");
		System.out.println("*  WELCOME TO REGISTRATION PAGE   : *");
		System.out.println("*                                   *");
		System.out.println("====================================");
		System.out.println("====================================");
		
		System.out.println("Enter Username : ");
		String username=keyboard.next();
		//validateExistingUser(LoggedInUser);
			
		users.setUsername(username);
		
		System.out.println("Enter Password :");
		String password=keyboard.next();
		users.setPassword(password);
		
		output.println(users.getUsername());//println from PrintWriter
		output.println(users.getPassword());
		
		System.out.println("User registered successfully.");
		output.close();
		
	}

	private static String String(Scanner input2) {
		// TODO Auto-generated method stub
		return null;
	}

	public static void welcomeScreen() {
		System.out.println("====================================");
		System.out.println("====================================");
		System.out.println("*                                   *");
		System.out.println("*      Welcome To LockMe.com   :    *");
		System.out.println("* Your personal digital locker      *");
		System.out.println("*                                   *");
		System.out.println("====================================");
		System.out.println("====================================");
	}
	
	//store options
	public static void storeCredentials(String LoggedInUser) {
		System.out.println("===========================================================");
		System.out.println("===========================================================");
		System.out.println("*                                                         *");
		System.out.println("*   WELCOME TO DIGITAL LOCKER STORE YOUR CREDS HERE  :    *");
		System.out.println("*                                                         *");
		System.out.println("===========================================================");
		System.out.println("===========================================================");
		
		userCredentials.setLoggedUser(LoggedInUser);
		
		System.out.println("Enter Site Name : ");
		String sitename=keyboard.next();
		userCredentials.setSiteName(sitename);
		
		System.out.println("Enter User name :");
		String username=keyboard.next();
		userCredentials.setUsername(username);
		
		System.out.println("Enter Password :");
		String password=keyboard.next();
		userCredentials.setPassword(password);
		
		lockerOutput.println(userCredentials.getLoggedUser());//check it
		lockerOutput.println(userCredentials.getSiteName());
		lockerOutput.println(userCredentials.getUsername());
		lockerOutput.println(userCredentials.getPassword());
			
		
		System.out.println("YOUR CREDENTIALS ARE STORED AND SECURED");
		lockerOutput.close();
		
		
	}
	
	//fetch credentials
	public static void fetchCredentials(String inpUserName) {
		System.out.println("====================================");
		System.out.println("====================================");
		System.out.println("*                                   *");
		System.out.println("*  WELCOME TO DIGITAL LOCKER        *");
		System.out.println("*  YOUR CREDENTIALS ARE   :         *");
		System.out.println("*                                   *");
		System.out.println("====================================");
		System.out.println("====================================");
		
		
		while(lockerinput.hasNext()) {
			if(lockerinput.next().equals(inpUserName)) {
				System.out.println(inpUserName);
				System.out.println("Site Name:" +lockerinput.next());
				System.out.println("User Name : " +lockerinput.next());
				System.out.println("User Password : " +lockerinput.next());
			}			
				
	}
	}
	
	
	public static void initApp() {
		File dbfile=new File("database.txt");//pointer creation to access file
		File lockerfile=new File("locker-file.txt");
		
		try {
			
			//read data from db file
			input=new Scanner(dbfile);
			//read data from locker file;
			lockerinput=new Scanner(lockerfile);
			//read data from keyboard
			keyboard=new Scanner(System.in);
			//output
			output=new PrintWriter(new FileWriter(dbfile,true));//FileWriter-to write into file,true-to append user details
			lockerOutput=new PrintWriter(new FileWriter(lockerfile,true));
			
			users=new Users();
			userCredentials=new UserSiteCredentials();
			
		} catch (IOException e) {
			System.out.println(" 404 : File Not Found ");
		}
		
		
		
		
}
}
