package com.simplilearn.lockme.model;

public class UserSiteCredentials {
	
	private String siteName;
	private String loggedUser;
	private String username;
	private String password;
	public UserSiteCredentials() {}
	
	public UserSiteCredentials(String siteName, String loggedUser, String username, String password) {
		super();
		this.siteName = siteName;
		this.loggedUser = loggedUser;
		this.username = username;
		this.password = password;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getLoggedUser() {
		return loggedUser;
	}

	public void setLoggedUser(String loggedUser) {
		this.loggedUser = loggedUser;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "UserSiteCredentials [siteName=" + siteName + ", loggedUser=" + loggedUser + ", username=" + username
				+ ", password=" + password + "]";
	}
	
	
	
	
	

}
